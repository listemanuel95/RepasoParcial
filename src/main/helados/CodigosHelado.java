package main.helados;

/**
 * <p>Clase auxiliar que almacena en constantes todos los c�digos de los sabores de helado.</p>
 */
public class CodigosHelado {

	public static final int HELADO_AMARENA = 0;
	public static final int HELADO_CHOCOLATE = 1;
	public static final int HELADO_CREMA_DEL_CIELO = 2;
	public static final int HELADO_DULCE_DE_LECHE = 3;
	public static final int HELADO_TRAMONTANA = 4;
	
}
