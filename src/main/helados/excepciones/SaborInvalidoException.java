package main.helados.excepciones;

/**
 * <p>Excepci�n llamada cuando se trata de acceder al mapa de helados con una clave inv�lida</p>
 */
public class SaborInvalidoException extends Exception {

	private static final long serialVersionUID = 1L;

	public SaborInvalidoException()
	{
		super("Sabor invalido.");
	}
	
}
