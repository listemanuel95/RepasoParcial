package main.helados.excepciones;

/**
 * <p>Excepci�n lanzada cuando se trata de comprar un sabor del que no hay stock suficiente.</p>
 */
public class StockInsuficienteException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public StockInsuficienteException(String msg)
	{
		super(msg);
	}
}
