package main.helados.excepciones;

/**
 * <p>Excepci�n llamada cuando se trata de acceder al mapa de envases con una clave inv�lida</p>
 */
public class EnvaseInvalidoException extends Exception {

	private static final long serialVersionUID = 1L;

	public EnvaseInvalidoException()
	{
		super("Envase invalido.");
	}
	
}
