package main.helados;

import java.util.HashMap;

/**
 * <p>La clase Helado representa un gusto de helado con su stock actual y su receta.</p>
 */
public class Helado {

	private double stock; // en kilos
	private HashMap<String, Integer> receta; // <Ingrediente, Cantidad>
	private String gusto;
	
	public Helado(int stock, HashMap<String, Integer> receta, String gusto)
	{
		this.stock = stock;
		this.receta = receta;
		this.gusto = gusto;
	}

	@Override
	public String toString()
	{
		return "[HELADO] " + gusto + " (" + stock + ") \n" + receta;
	}
	
	public void bajarStock(double d)
	{
		stock -= d;
	}
	
	public double getStock() 
	{
		return stock;
	}

	public HashMap<String, Integer> getReceta() 
	{
		return receta;
	}

	public String getGusto() 
	{
		return gusto;
	}
	
}
