package main.helados.envases;

/**
 * <p>Clase auxiliar que almacena en constantes todos los c�digos de los tipos de envase.</p>
 */
public class CodigosEnvase {

	public static final int ENVASE_KILO = 0;
	public static final int ENVASE_MEDIO = 1;
	public static final int ENVASE_CUARTO = 2;
	public static final int ENVASE_CUCURUCHO = 3;
	
}
