package main.helados.envases;

/**
 * <p>Almacena los datos de todos los tipos de envase que maneja la helader�a.</p>
 */
public class Envase {

	private String etiqueta;
	private int precio;
	private int cantSabores;
	private double cantidad;
	
	public Envase(String etiqueta, int precio, double cantidad, int cantSabores)
	{
		this.etiqueta = etiqueta;
		this.precio = precio;
		this.cantidad = cantidad;
		this.cantSabores = cantSabores;
	}

	public String getEtiqueta() 
	{
		return etiqueta;
	}

	public int getPrecio() 
	{
		return precio;
	}

	public double getCantidad() 
	{
		return cantidad;
	}

	public int getCantSabores()
	{
		return cantSabores;
	}
}
