package main;

import java.util.HashMap;

import org.json.JSONObject;

import main.heladeria.Heladeria;
import main.helados.CodigosHelado;
import main.helados.Helado;
import main.helados.envases.CodigosEnvase;
import main.helados.envases.Envase;
import main.helados.excepciones.EnvaseInvalidoException;
import main.helados.excepciones.SaborInvalidoException;
import main.helados.excepciones.StockInsuficienteException;

public class Main {

	public static void main(String[] args)
	{
		// inicializamos la helader�a (inicializa vectores)
		Heladeria.init();
		
		// creamos y agregamos los envases
		Heladeria.agregarEnvase(CodigosEnvase.ENVASE_KILO, new Envase("1 K", 200, 1, 4));
		Heladeria.agregarEnvase(CodigosEnvase.ENVASE_MEDIO, new Envase("1/2 K", 120, 0.5, 3));
		Heladeria.agregarEnvase(CodigosEnvase.ENVASE_CUARTO, new Envase("1/4 K", 80, 0.25, 3));
		Heladeria.agregarEnvase(CodigosEnvase.ENVASE_CUCURUCHO, new Envase("Cucurucho", 60, 0.2, 2));
		
		// agregamos helados
		HashMap<String, Integer> receta = new HashMap<String, Integer>();
		receta.put("Cereza", 5);
		receta.put("Crema", 2);
		receta.put("Leche", 10);
		Helado hAmarena = new Helado(10, receta, "Amarena");
		Heladeria.agregarHelado(CodigosHelado.HELADO_AMARENA, hAmarena);
		
		receta = new HashMap<String, Integer>();
		receta.put("Cacao", 3);
		receta.put("Crema", 2);
		receta.put("Leche", 10);
		Helado hChocolate = new Helado(10, receta, "Chocolate");
		Heladeria.agregarHelado(CodigosHelado.HELADO_CHOCOLATE, hChocolate);
		
		receta = new HashMap<String, Integer>();
		receta.put("Az�car", 8);
		receta.put("Crema", 2);
		receta.put("Leche", 10);
		Helado hDDL = new Helado(10, receta, "Dulce de Leche");
		Heladeria.agregarHelado(CodigosHelado.HELADO_DULCE_DE_LECHE, hDDL);
		
		receta = new HashMap<String, Integer>();
		receta.put("Colorante", 1);
		receta.put("Az�car", 5);
		receta.put("Crema", 2);
		receta.put("Leche", 10);
		Helado hCDC = new Helado(10, receta, "Crema del Cielo");
		Heladeria.agregarHelado(CodigosHelado.HELADO_CREMA_DEL_CIELO, hCDC);
		
		Heladeria.mostrarStocks();
		
		try {
			Heladeria.ventaRandom();
			Heladeria.ventaRandom();
			Heladeria.ventaRandom();
		} catch (EnvaseInvalidoException | SaborInvalidoException | StockInsuficienteException e) {
			e.printStackTrace();
		}
	
		System.out.println();
		Heladeria.mostrarVentas();
		System.out.println();
		Heladeria.mostrarStocks();
		
		JSONObject json = Heladeria.exportarJSON();
		System.out.println("\n" + json.toString(4));
		
		Heladeria.generarArchivoMontosDeVenta();
		System.out.println();
		Heladeria.leerArchivoMontosDeVenta();
	}
	
}
