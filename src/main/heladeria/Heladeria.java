package main.heladeria;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;

import org.json.JSONArray;
import org.json.JSONObject;

import main.heladeria.venta.Venta;
import main.helados.Helado;
import main.helados.envases.Envase;
import main.helados.excepciones.EnvaseInvalidoException;
import main.helados.excepciones.SaborInvalidoException;
import main.helados.excepciones.StockInsuficienteException;

/**
 * <p>Clase est�tica que maneja todo el funcionamiento del programa</p>
 */
public class Heladeria {

	private static HashMap<Integer, Helado> helados; // mapa de helados
	private static HashMap<Integer, Envase> envases; // mapa de envases
	private static ArrayList<Venta> ventas; // arreglo de ventas
	
	public static void init()
	{
		helados = new HashMap<Integer, Helado>();
		envases = new HashMap<Integer, Envase>();
		ventas = new ArrayList<Venta>();
	}
	
	/**
	 * <p>Agrega un helado al mapa de helados</p>
	 * @param id	&emsp;(<b>int</b>) ID del helado (ver {@link main.helados.CodigosHelado CodigosHelado})
	 * @param h		&emsp;({@link main.helados.Helado Helado}) el helado a agregar
	 */
	public static void agregarHelado(int id, Helado h)
	{
		helados.put(id, h);
	}
	
	/**
	 * <p>Agrega un envase al mapa de envases</p>
	 * @param id	&emsp;(<b>int</b>) ID del envase (ver {@link main.helados.envases.CodigosEnvase CodigosEnvase})
	 * @param e		&emsp;({@link main.helados.envases.Envase Envase}) el envase a agregar
	 */
	public static void agregarEnvase(int id, Envase e)
	{
		envases.put(id, e);
	}
	
	/**
	 * <p>Muestra todos los helados cuyo stock es menor al valor enviado por par�metro</p>
	 * @param stock		&emsp;(<b>int</b>) el stock a verificar
	 */
	public static void mostrarStockMenorA(int stock)
	{
		// iteramos el mapa
		Iterator<Entry<Integer, Helado>> it = helados.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Integer, Helado> pair = (Map.Entry<Integer, Helado>) it.next();
	        
	        // casteamos a Helado
	        Helado h = (Helado) pair.getValue();
	        
	        // mostramos
	        if(h.getStock() < stock)
	        	System.out.println(h);
	    }
	}
	
	/**
	 * <p>Salida por pantalla heladero-friendly</p>
	 */
	public static void listarHeladero()
	{
		// iteramos el mapa
		Iterator<Entry<Integer, Helado>> it = helados.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Integer, Helado> pair = (Map.Entry<Integer, Helado>) it.next();
	        
	        // casteamos a Helado
	        Helado h = (Helado) pair.getValue();
	        
	        // mostramos
	        System.out.println(h);
	    }
	}
	
	/**
	 * <p>Salida por pantalla proveedor-friendly</p>
	 */
	public static void listarProveedor()
	{
		// iteramos el mapa
		Iterator<Entry<Integer, Helado>> it = helados.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Integer, Helado> pair = (Map.Entry<Integer, Helado>) it.next();
	        
	        // casteamos a Helado
	        Helado h = (Helado) pair.getValue();
	        
	        // mostramos
	        System.out.println(pair.getKey() + ", " + h.getGusto());
	    }
	}
	
	/**
	 * <p>Genera una venta aleatoria</p>
	 * @throws EnvaseInvalidoException
	 * @throws SaborInvalidoException
	 * @throws StockInsuficienteException
	 */
	public static void ventaRandom() throws EnvaseInvalidoException, SaborInvalidoException, StockInsuficienteException
	{
		// elegimos aleatoriamente el envase que compra
		int envaseAleatorio = ThreadLocalRandom.current().nextInt(0, envases.size());
		if(envases.get(envaseAleatorio) == null)
			throw new EnvaseInvalidoException();
		
		// elegimos aleatoriamente los gustos
		ArrayList<String> gustos = new ArrayList<String>();
		for(int i = 0; i < envases.get(envaseAleatorio).getCantSabores(); i++)
		{
			int gustoAleatorio = ThreadLocalRandom.current().nextInt(0, helados.size());
			if(helados.get(gustoAleatorio) == null)
				throw new SaborInvalidoException();
			
			// chequeamos si alcanza el stock (si compr� 1/4, cada gusto -3- ocupa un tercio de ese cuarto, es decir: 0.25 / 3; as� con todos)
			double cantidad = envases.get(envaseAleatorio).getCantidad() / envases.get(envaseAleatorio).getCantSabores();
			
			if(helados.get(gustoAleatorio).getStock() < cantidad)
				throw new StockInsuficienteException("No hay stock suficiente del helado" + helados.get(gustoAleatorio).getGusto());
			
			// bajamos el stock 
			helados.get(gustoAleatorio).bajarStock(cantidad);
			
			// no agregamos repetidos
			if(!gustos.contains(helados.get(gustoAleatorio).getGusto()))
				gustos.add(helados.get(gustoAleatorio).getGusto());
		}
		
		// creamos la venta
		Venta v = new Venta(envases.get(envaseAleatorio).getEtiqueta(), LocalDate.now(), envases.get(envaseAleatorio).getPrecio(), gustos);
	
		// la agregamos al arreglo
		ventas.add(v);
	}
	
	/**
	 * <p>Muestra por pantalla las ventas realizadas hasta el momento</p>
	 */
	public static void mostrarVentas()
	{
		System.out.println("VENTAS: ");
		
		for(Venta v : ventas)
			System.out.println(v);
	}

	/**
	 * <p>Muestra por pantalla los stocks de cada helado</p>
	 */
	public static void mostrarStocks()
	{
		System.out.println("STOCKS: ");
		
		// iteramos el mapa
		Iterator<Entry<Integer, Helado>> it = helados.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Integer, Helado> pair = (Map.Entry<Integer, Helado>) it.next();
	        
	        // casteamos a Helado
	        Helado h = (Helado) pair.getValue();
	        
	        // mostramos
	        System.out.println(h.getGusto() + " (" + h.getStock() + ")");
	    }
	}
	
	/**
	 * <p>Parsea los datos de cada helado a un JSONObject y lo retorna</p>
	 */
	public static JSONObject exportarJSON()
	{
		JSONObject ret = new JSONObject();
		JSONArray heladosARR = new JSONArray();
		
		// iteramos el mapa
		Iterator<Entry<Integer, Helado>> it = helados.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Integer, Helado> pair = (Map.Entry<Integer, Helado>) it.next();
	        
	        // casteamos a Helado
	        Helado h = (Helado) pair.getValue();
	        
	        // creamos el objeto JSON
	        JSONObject heladoOBJ = new JSONObject();
	        heladoOBJ.put("sabor", h.getGusto());
	        heladoOBJ.put("stock", h.getStock());
	        
	        // lcreamos la receta como un array
	        JSONArray recetasARR = new JSONArray();
	        
	        // iteramos la receta
	        Iterator<Entry<String, Integer>> otroit = h.getReceta().entrySet().iterator();
		    while (otroit.hasNext()) {
		        Map.Entry<String, Integer> par = (Map.Entry<String, Integer>) otroit.next();
		        
		        // creamos un objeto para cada receta
		        JSONObject rec = new JSONObject();
		        rec.put("ingrediente", par.getKey());
		        rec.put("cantidad", par.getValue());
		        
		        // agregamos al array json
		        recetasARR.put(rec);
		    }
	        
	        // agregamos la receta al helado
		    heladoOBJ.put("receta", recetasARR);
	        
	        // agregamos al array
		    heladosARR.put(heladoOBJ);
	    }
	    
	    ret.put("helados", heladosARR);
		
		return ret;
	}
	
	/**
	 * <p>Genera un archivo binario con el importe de cada venta</p>
	 */
	public static void generarArchivoMontosDeVenta()
	{
		FileOutputStream fos;
		try {
			fos = new FileOutputStream("dat/ventas.dat");
			DataOutputStream dos = new DataOutputStream(fos);
			
			// recorremos las ventas
			for(Venta v : ventas)
				dos.writeInt(v.getPrecio());
				
			dos.close();
			fos.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * <p>Lee el archivo de montos de venta</p>
	 */
	public static void leerArchivoMontosDeVenta()
	{
		FileInputStream fis;
		
		try {
			fis = new FileInputStream("dat/ventas.dat");

			DataInputStream dis = new DataInputStream(fis);
			
			while(dis.available() > 0)
				System.out.println("VENTA: $" + dis.readInt());
			
			dis.close();
			fis.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
