package main.heladeria.venta;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * <p>Clase que almacena los datos de cada venta de la helader�a.</p>
 */
public class Venta {

	private String etiquetaEnvase;
	private LocalDate fecha;
	private int precio;
	private ArrayList<String> sabores;
	
	public Venta(String etiquetaEnvase, LocalDate fecha, int precio, ArrayList<String> sabores)
	{
		this.etiquetaEnvase = etiquetaEnvase;
		this.fecha = fecha;
		this.precio = precio;
		this.sabores = sabores;
	}

	@Override
	public String toString()
	{
		return "[VENTA] " + fecha + ", " + etiquetaEnvase + ", ($" + precio + ")\n" + sabores;
	}
	
	public String getEtiquetaEnvase() 
	{
		return etiquetaEnvase;
	}

	public LocalDate getFecha() 
	{
		return fecha;
	}

	public int getPrecio() 
	{
		return precio;
	}

	public ArrayList<String> getSabores() 
	{
		return sabores;
	}
}
